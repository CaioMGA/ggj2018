﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barco : Toy {
	Rigidbody _rb;

	void Start(){
		_rb = GetComponent<Rigidbody> ();
	}

	public override void TurnLeft(){
		transform.Rotate (0, -turnSpeed, 0);
	}

	public override void TurnRight(){
		transform.Rotate (0, turnSpeed, 0);
	}

	public override void Forward(){
		_rb.AddRelativeForce (Vector3.forward * speed);
	}

	public override void Backward(){
		_rb.AddRelativeForce (Vector3.forward * -speed);
	}
}
