﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalReceiver : MonoBehaviour {
	Toy _toy;

	void Start(){
		_toy = GetComponent<Toy> ();
	}

	void OnTriggerEnter(Collider other){
		if(other.transform.CompareTag("Signal")){
			_toy.executeCommands();
			Destroy (other.gameObject);
		}
	}

}
