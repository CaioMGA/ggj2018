﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraControl : MonoBehaviour {
	public CinemachineVirtualCamera _static_center;
	public CinemachineVirtualCamera _static_corner;
	public CinemachineVirtualCamera _target_group;
	public CinemachineVirtualCamera _locked_on_toy;

	public void showLockedOnToy(){
		_static_center.enabled = false;
		_static_corner.enabled = false;
		_target_group.enabled = false;
		_locked_on_toy.enabled = true;
	}

	public void showTargetGroup(){
		_static_center.enabled = false;
		_static_corner.enabled = false;
		_target_group.enabled = true;
		_locked_on_toy.enabled = false;
	}

	public void showStaticCenter(){
		_static_center.enabled = true;
		_static_corner.enabled = false;
		_target_group.enabled = false;
		_locked_on_toy.enabled = false;
	}

	public void showStaticCorner(){
		_static_center.enabled = false;
		_static_corner.enabled = true;
		_target_group.enabled = false;
		_locked_on_toy.enabled = false;
	}

}
