﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tanque : Toy {
	public WheelCollider dianteira_L;
	public WheelCollider dianteira_R;
	public WheelCollider traseira_L;
	public WheelCollider traseira_R;

	public int a;

	Rigidbody _rb;

	void Start(){
		_rb = GetComponent<Rigidbody> ();
	}

	public override void TurnLeft(){
		dianteira_L.motorTorque = -speed * 2;
		dianteira_R.motorTorque = speed * 2;
		traseira_L.motorTorque = -speed * 2;
		traseira_R.motorTorque = speed * 2;
	}

	public override void TurnRight(){
		dianteira_L.motorTorque = speed * 2;
		dianteira_R.motorTorque = -speed * 2;
		traseira_L.motorTorque = speed * 2;
		traseira_R.motorTorque = -speed * 2;
	}

	public override void Forward(){
		dianteira_L.motorTorque = speed;
		dianteira_R.motorTorque = speed;
		traseira_L.motorTorque = speed;
		traseira_R.motorTorque = speed;
	}

	public override void Backward(){
		dianteira_L.motorTorque = -speed;
		dianteira_R.motorTorque = -speed;
		traseira_L.motorTorque = -speed;
		traseira_R.motorTorque = -speed;
	}

}
