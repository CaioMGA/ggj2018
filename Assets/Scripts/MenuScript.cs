﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	public void LoadScene(string sceneName){
		SceneManager.LoadScene (sceneName);
	}

	public void Show(GameObject go){
		go.SetActive (true);
	}

	public void Hide(GameObject go){
		go.SetActive (false);
	}

	public void ToggleInteractable(Button btn){
		btn.interactable = !btn.interactable;
	}

	public void Toggle(GameObject go){
		go.SetActive (!go.activeInHierarchy);
	}

	public void Quit(){
		Application.Quit ();
	}

	public void OpenURL(string url){
		Application.OpenURL (url);
	}
}
