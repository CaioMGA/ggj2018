﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toy : MonoBehaviour {
	//classe genérica de brinquedo. Os métodos devem ser sobrecarregados
	public float speed;
	public float turnSpeed;
	public bool isOn = false;
	List<CmdToy> commands;

	public void AddCommand(CmdToy cmd){
		if(this.commands == null){
			this.commands = new List<CmdToy> ();
		}
		this.commands.Add (cmd);
	}

	public void executeCommands(){
		if(commands.Count > 0){
			commands [0].execute (this);
			commands.Remove (commands[0]);
		}
	}

	public virtual void TurnLeft(){
		Debug.Log ("isso deveria ter sido sobrecarregado: TurnLeft");
	}

	public virtual void TurnRight(){
		Debug.Log ("isso deveria ter sido sobrecarregado: TurnRight");
	}

	public virtual void GoUp(){
		Debug.Log ("isso deveria ter sido sobrecarregado: GoUp");
	}

	public virtual void GoDown(){
		Debug.Log ("isso deveria ter sido sobrecarregado: GoDown");
	}

	public virtual void Forward(){
		Debug.Log ("isso deveria ter sido sobrecarregado: Forward");
	}

	public virtual void Backward(){
		Debug.Log ("isso deveria ter sido sobrecarregado: Backward");
	}

	public virtual void TogglePower(){
		Debug.Log ("isso deveria ter sido sobrecarregado: TogglePower");
	}
}
