﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heitor : MonoBehaviour {

	public Toy _toy;
	public GameObject signal;

	void Update(){
		//Testes apenas
		if(Input.GetKeyDown(KeyCode.A)){
			GameObject s = Instantiate (signal);
			s.transform.position = transform.position;
			_toy.AddCommand(new ToyTurnLeft());
		} else if(Input.GetKeyDown(KeyCode.D)){
			GameObject s = Instantiate (signal);
			s.transform.position = transform.position;
			_toy.AddCommand(new ToyTurnRight());
		} else if(Input.GetKeyDown(KeyCode.W)){
			GameObject s = Instantiate (signal);
			s.transform.position = transform.position;
			_toy.AddCommand(new ToyForward());
		} else if(Input.GetKeyDown(KeyCode.S)){
			GameObject s = Instantiate (signal);
			s.transform.position = transform.position;
			_toy.AddCommand(new ToyBackward());
		} else if(Input.GetKeyDown(KeyCode.Space)){
			_toy.executeCommands ();
		}
	}
}
